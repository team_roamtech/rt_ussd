<?php

namespace App\Http\Controllers\Download;

use App\Content\ContentDownload;
use App\Http\Controllers\Content\ContentDetail;
use App\Http\Controllers\Bonyeza\Content;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Index extends Controller
{
    public function create( $data)
    {
       $response = new ContentDownload();
       $response= $response->setConnection('bonyeza');
       $response = $response->create($data);
       return $response;
    }
    public function download_music($clause)
    {
        $content = new  ContentDetail();
        $content_detail = $content->fetch_content_detail($clause);
        $disk = Storage::disk('gcs');
        $file = $disk->download("/rt_ussd/".$content_detail->bucket_name);
        return $file;
    }
    public function download_v2($param) 
    {
    //   //  $bonyeza = new  Content();
    //   //  $response = $bonyeza->fetch_uri($param);
    //   //  $good_data = json_decode($response->content_uri);
    //     $link = 'https://storage.googleapis.com/btunes-storage/contents/September2020/LT0AG9Z4GonhlirWiX1yoXq6Z7m9kvSsCb78xdYZ.mp4';
    //     $disk = Storage::disk('bonyeza');
    //     $disk->setVisibility($link, 'public');
    //     $file = $disk->download($link);
    //    return $file;
    $bonyeza = new  Content();
        $response = $bonyeza->fetch_uri($param);
        $filename = "$response->title".".mp3";
        $tempImage = tempnam(sys_get_temp_dir(), $filename);
        $good_data = json_decode($response->content_uri);
        $link = 'https://storage.googleapis.com/btunes-storage/'.$good_data[0]->download_link;
       
    //   $link = "https://storage.googleapis.com/btunes-storage/contents/1/Monie k-Murunaguo.mp3";
       $link = str_replace(' ', '%20', $link);
    
        copy($link, $tempImage); 
        return response()->download($tempImage, $filename);


    }
}
