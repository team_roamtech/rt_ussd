<?php

namespace App\Http\Controllers\Download;

use App\Http\Controllers\Bonyeza\Content;
use App\Http\Controllers\Download\Index as DownloadIndex;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class App extends Controller
{
    public function register_music(Request $request)
    {
        $rules = [
            'uri' => 'required',
        ];
        $this->validate($request,$rules);
        $data['uri']=$request['uri'];
        $content = new DownloadIndex();
        $response = $content->create($data);
        $request['download_id'] = $response->id;
        return response()->json($request,200);
;    }
}
