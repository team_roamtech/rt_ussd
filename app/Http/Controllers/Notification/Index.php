<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use GuzzleHttp\Client;


class Index extends Controller
{
    public  function send_sms_notification($request)
    {
        $token = \App\Http\Controllers\Auth\Index::safaricom_token();
        $uniq  = uniqid();
        $url = "https://dsvc.safaricom.com:9480/api/public/CMS/bulksms";
        $requestBody = json_encode( [
            "timeStamp" =>  time(),
                "dataSet"=>array( array(
                     'userName'  => 'roamtech',
                     'channel'   => 'sms' ,
                     'packageId' => '4557',
                     'oa'        =>  'ROAMTECH',
                     'msisdn'    =>   '254721520331,254705744566,254797561830,254722930828',
                     'message'   =>  $request['message'] . " TO  BE SENT  AT ". $request['send_time'] ,
                     'uniqueId'  => $uniq,
                     'actionResponseURL'  => 'http://webhook.site/dde7a0d7-92dc-41bc-91d9-3049c01081fd'
                ))
        ]);
      //  dd($requestBody);
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Host: dsvc.safaricom.com:9480',
            'X-Authorization: Bearer ' . $token
        ));
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $requestBody);
        $response = curl_exec($handle);
        return $response;
       
    }
}
