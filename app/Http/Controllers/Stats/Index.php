<?php

namespace App\Http\Controllers\Stats;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Index extends Controller
{
     public function fetch_stats($id)
     {
       /* Delivered */
       $response =   DB::table('dtt_outboxes')
         ->select('*')
         ->where([
             ['blasts_id','=',$id]
         ])
         ->count();
         $stats['delivered']  = $response;
         /* Total   Sent  */
         $response =   DB::table('blasts')
         ->select('total_sent')
         ->where([
             ['id','=',$id]
         ])
         ->first();
         $stats['total_sent']  = $response->total_sent;
         if ($response->total_sent == 0)
         {
           $stats['delivered'] = 0;
           $stats['total_sent'] = 0;
           $stats['percentage'] = '0%';
           return response()->json($stats,200);
         }else {
          $stats['percentage'] = round($stats['delivered']/ $stats['total_sent']*100,2);
          return response()->json($stats,200);
         }    
     }
}
