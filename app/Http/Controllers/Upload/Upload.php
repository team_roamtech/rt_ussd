<?php

namespace App\Http\Controllers\Upload;

use App\Content\Content as ContentContent;
use App\Content\ContentDetail;
use App\Http\Controllers\Content\Content;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Functions\Index;
use Illuminate\Http\Request;

class Upload extends Controller
{
    public function upload(Request $request)
    {
        $rules = [
            'file' => 'required',
            'content_types_id' => 'required',
            'shortcode_services_id'=>'required',
            'ussds_id' => 'required',
            'customer_account' => 'required',
            'users_id'  => 'required'
        ];
        $this->validate($request,$rules);
        /* Create Content  Detail ID */
        $content_detail = new \App\Http\Controllers\Content\ContentDetail();
        $request  = $content_detail->create($request);
        $request['link'] = "https://content.emalify.com:9997/api/d/".$request['download_name'];
        $response = self::create($request);
        return $response;
    }
    public function  create($request)
    {
     $data['content_details_id'] = $request['content_details_id'];
     $data['content_types_id'] = $request['content_types_id'];
     $data['shortcode_services_id'] = $request['shortcode_services_id'];
     if(isset($request['ussds_id']))
     {
         $data['ussds_id'] = $request['ussds_id'];
     }
     $data['customer_account'] = $request->customer_account;
     $data['content_name'] = $request->content_name;
     $data['users_id'] = $request->users_id;
     $data['link'] = $request->link;
     $response = self::store($data);
     return $response;
    }
    public function  store($data)
    {
       $response =  \App\Content\Content::create($data);
       return $response;
    }

}
