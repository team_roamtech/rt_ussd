<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Functions\Index;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContentDetail extends Controller
{
    public  function  create($request)
    {
        $request['original_name'] = $request->file('file')->getClientOriginalName();
        $request['extension'] = $request->file('file')->getClientOriginalExtension();
        $request['size'] = $request->file('file')->getSize();
        $bucket_name = '699_' . time() . "." . $request['extension'];
        $request['bucket_name'] = $bucket_name;
        $path = $request->file('file')->storeAs('rt_ussd', $bucket_name,'gcs');
        $request['download_name'] = Index::generateString();
        $request = self::store($request);
        return $request;
    }
    public  function  store($request)
    {
        $data['original_name'] = $request->original_name;
        $data['extension'] = $request->extension;
        $data['bucket_name'] = $request->bucket_name;
        $data['size'] = $request->size;
        $data['download_name'] = $request->download_name;
        $content_detail = \App\Content\ContentDetail::create($data);
        if($content_detail->id)
        {
            $request['content_details_id'] = $content_detail->id;
            return $request;
        }
    }
    public function  fetch_content_detail($info)
    {
        $content_details =
        
         DB::table('content_details')
        ->select('*')
        ->where([
             ['download_name','=',$info]
             ])
        ->first();
        return $content_details;
    }
}
