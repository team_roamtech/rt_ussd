<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Model\ContactGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContentType extends Controller
{
public  function  store(Request $request)
{
    $rules = [
        'type' => 'required',
        'customer_account' =>'required'
    ];
    $this->validate($request,$rules);
    $response = self::create($request);
    return $response;
}
public  function  create($request)
{
    $data['type'] = $request->type;
    $data['customer_account'] = $request->customer_account;
    $resource = \App\Content\ContentType::create($data);
    return $resource;
}
public  function  read()
{
   $content_types =  DB::table('content_types')
                     ->select('*')
                     ->get();
   return response()->json($content_types,200);
}
public  function  delete(Request $request)
{
    $rules = [ 'content_type_id' => 'required',];
    $this->validate($request,$rules);
    $instance = DB::table('content_types')
                ->where([[ 'id' ,'=',$request->content_type_id]])
               ->delete();
    if ($instance > 0 )
    {
        return response()->json('Record deleted successfully',200);
    }else {
        return response()->json('Error ! Failure  to delete  record',500);
    }
}
public function fetch_all()
{
    $content_types= DB::table('content_types')
    ->select('type','id')
    ->get();
    return $content_types;
}
}
