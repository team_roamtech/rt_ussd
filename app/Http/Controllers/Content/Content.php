<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Upload\Upload;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Content extends Controller
{
    public function  fetch_all()
    {
      $response =   DB::table('contents')
        ->select('sdp_name','ussd_code','type','original_name','full_name','link','content_name','contents.created_at')
        ->join('shortcode_services','contents.shortcode_services_id','=','shortcode_services.id')
        ->join('content_details','contents.content_details_id','=','content_details.id')
        ->join('ussds','contents.ussds_id','=','ussds.id')
        ->join('users','contents.users_id','=','users.id')
        ->join('content_types','contents.content_types_id','=','content_types.id')
        ->orderBy('contents.created_at','desc')
        ->get();
        return response()->json($response,200);
    }
}
