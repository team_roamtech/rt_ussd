<?php

namespace App\Http\Controllers\Ussd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Ussd extends Controller
{
    public  function  store(Request $request)
    {
        $rules = [
            // 'original_name' => 'required|unique:content_details'
             'ussd_code' => 'required|unique:ussds',
             'customer_account' => 'required'
        ];
        $this->validate($request,$rules);
        $data['ussd_code'] = $request->ussd_code;
        $data['customer_account'] = $request->customer_account;
        $response = \App\Ussd\Ussd::create($data);
        return $response;
    }
    public  function   fetch_all()
    {
        $ussds = DB::table('ussds')
            ->select('ussd_code','id')
           ->get();
       return response()->json($ussds,200);
    }
    public  function  read_per_acc($customer_account)
    {
       $ussd =  DB::table('ussds')
            ->select('*')
            ->where( [
                ['customer_account','=' , $customer_account]
                    ]
            )
            ->paginate('5');
       return response($ussd,200);
    }
}
