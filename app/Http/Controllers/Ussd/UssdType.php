<?php

namespace App\Http\Controllers\Ussd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UssdType extends Controller
{
    public  function  store(Request $request)
    {
        $rules = [
            'type' => 'required',
        ];
        $this->validate($request,$rules);
       $data['type'] = $request->type;
       $response = self::create($data);
       return response()->json($response,200);
    }
    public  function  create($data)
    {
        $ussd_type =new \App\Ussd\UssdType();
        $response = $ussd_type->create($data);
        $request['ussd_type_id'] = $response->id;
        return response()->json($request,200);
    }
}
