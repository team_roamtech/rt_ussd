<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class RtGateway extends Controller
{
    public static function getClientToken()
    {
        return Cache::remember("api_accesstoken",1400,function (){
            $client = new Client(['headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ]
            ]);
            $url = "https://api.emalify.com/v1/oauth/token";
            $body['client_id'] = "OwWmrb7h3ZOoxIysSiYjaczMrnyQ9YWA";
            $body['client_secret'] = "tBVqu5GLcT4DrdqLUSuPgjoBIgQ3GnUJ2C4689Ea";
            $body['grant_type'] = "client_credentials";
            $res = $client->post($url, ['json' => $body]);
            $res = json_decode($res->getBody()->getContents());
            return $res->access_token;
        });
    }
}
