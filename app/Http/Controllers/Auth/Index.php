<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use GuzzleHttp\Client;

class Index extends Controller
{
    public  static function  safaricom_token()
    {
        return Cache::remember("safaricom_access_token",900,function (){
            $client = new Client(['headers' => [
                 'Accept' => 'application/json',
                 'Content-Type' => 'application/json',
                 'X-Requested-With' => 'XMLHttpRequest',
            ]
            ]);
            $url = "https://dsvc.safaricom.com:9480/api/auth/login";
            $body = [
                'username' => "roamtechapi",
                'password' => "ROAMTECHAPI@ps7894",
            ];
            $res = $client->post($url, ['json' => $body, 'verify' => false]);
            $response = $res->getBody()->getContents();
            $token_object =  json_decode($response);
            return $token_object->token;
        });
    }
}
