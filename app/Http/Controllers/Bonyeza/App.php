<?php

namespace App\Http\Controllers\Bonyeza;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class App extends Controller
{

    public function generate_link()
    {
        $count =   DB::connection('bonyeza')
        ->table('contents')
        ->select('*')
        ->count();
        
        for ($x = 1; $x <= $count; $x++) {
            $s = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);

            
            $response  = DB::connection('bonyeza')
            ->table('contents')
            ->where('id','=',$x)
            ->update( [
                       'uri' => $s ,
            ] );
          } 
    }
}
