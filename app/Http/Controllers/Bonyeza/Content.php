<?php

namespace App\Http\Controllers\Bonyeza;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Psy\Command\WhereamiCommand;

class Content extends Controller
{
    public function fetch_by_id($user_id)
    {
        
           $response =   DB::connection('bonyeza')
                      ->table('contents')
                      ->select('id','uri','content_uri','title AS song')
                      ->where([
                          ['user_id','=',$user_id]
                      ])
                      ->get();
            foreach ($response as $bad_data)
            {
                
                $good_data = json_decode($bad_data->content_uri);
                $song[] =array('id' => $bad_data->id, 'song' => $bad_data->song, 'link' => 'http://www.btunes.me/'.$bad_data->uri);    
            }
            return response()->json($song,200);
        }
        public function fetch_content_by_id($request)
        {
            
               $response =   DB::connection('bonyeza')
                          ->table('contents')
                          ->select('id','uri','content_uri','title AS song')
                          ->where([
                              ['id','=',$request->content_id]
                          ])
                           ->first();
                    
                   $good_data = json_decode($response->content_uri);
                  $request['link'] = 'http://www.btunes.me/'.$response->uri;
                  $request['title'] = $response->song;
               
                 return $request;
            }
            public function  fetch_uri($param)
            { 
                $response = DB::connection('bonyeza')
                            ->table('contents')
                            ->select('*')
                            ->where([ 
                                   ['uri','=',$param]
                                  ])
                            ->first();
                        
                return $response;
            }
}
