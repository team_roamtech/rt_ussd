<?php

namespace App\Http\Controllers\Bonyeza;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Artist extends Controller
{
     public function all ()
     {
        $users =   DB::connection('bonyeza')
                   ->table('users')
                   ->select('id','aka as name')
                   ->get();
         return  response()->json($users,200);
     }
     public function fetch_artist_by_id($request)
     {
        $users =   DB::connection('bonyeza')
                   ->table('users')
                   ->select('id','aka')
                   ->where([
                        ['id','=',$request->artist_id]
                   ])
                   ->first();
          $request['artist_name'] = $users->aka;
          return $request;
     }
}
