<?php

namespace App\Http\Controllers\Blast;

use App\Http\Controllers\Auth\RtGateway;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Helper extends Controller
{
       
    public function   fetch_service_id ($request)
    {
       $offer_code=  DB::table('shortcode_services')
        ->select('service_id')
        ->where([
            ['offer_code','=',$request->offer_code]
        ])
        ->first();
        $request->service_id = (integer) $offer_code->service_id;
        return $request ;
    }
    public function validate_offercode($request)
    {
        // select offer_code from shortcodes  inner  join  shortcode_services on  shortcodes.id = shortcode_services.shortcodes_id and  shortcode='21026';
       $offer_codes=DB::table('shortcodes')
        ->select('offer_code')
        ->join('shortcode_services','shortcode_services.shortcodes_id','=','shortcodes.id')
        ->whereIn('shortcode', ['21026','40292'])
        ->get();
        $array_offer_code=$offer_codes->map(function($value) { return $value->offer_code; })->all();
        $return = in_array($request->offer_code,$array_offer_code);
        $request['offer_code_presence']= $return;
        return $request;
    }
    public function publish_to_mq($request)
    {
        try {
            $client = new Client(['headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ]
            ]);
            $url = "http://35.241.180.246:29122/api/v1/enqueue/scheduled/";
            $body['blast_id']    =(integer) $request->blasts_id;
            $body['service_id']  = $request->service_id;
            $body['offer_code'] = $request->offer_code;
            $body['message'] = $request->message;
            $body['send_time'] = $request->send_time;
            $res = $client->post($url, ['json' => $body]);
            $response = json_decode($res->getBody()->getContents());
            
        }catch (ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            $error = json_decode($responseBodyAsString);
            return $error->message;
        }
    }
    public  function   fetch_subs($request)
    {
        /**Test */
        $request->message = addslashes($request->message);
        $file_name = "CONTENT_".time().".tsv";
       $path = "/var/data/content/$file_name";
        $request->file_name = $path;
        

        $sql ="SELECT  
        $request->id AS blasts_id,CONCAT($request->id,'_',msisdn,'_',0)reference_number,
        msisdn  AS  msisdn, '{$request->message}' AS message, 'PROCESSED' AS status ,'0' AS number_of_retries,
        now()  created_at, now() updated_at 
        FROM shortcode_services
        JOIN  clean_subs  ON  clean_subs.offer_code COLLATE utf8mb4_unicode_ci = shortcode_services.offer_code
        WHERE  shortcode_services.offer_code   = {$request->offer_code}";
    
        $request->subs_sql= $sql;
        Log::info("File Send Sms :: SQL Found For Processing $sql");
        return $request;
    }
    public   static    function  fetch_total_sent($request)
    {
        $word_count=shell_exec("tr ' ' '\n' <  $request->file_path | sort | uniq | wc -l");
        $request->total_sent =trim($word_count)-1;
        return $request;
    }
    // test message
    public  function   fetch_not_sent_blast()
    {
       $blasts =  DB::table('blasts')
                  ->select('*') 
                  ->whereRaw('send_time BETWEEN DATE_ADD(NOW(), INTERVAL -1 DAY) AND DATE_ADD(NOW(), INTERVAL 45 MINUTE)')
                  ->where([ ['status','=','NOT_SENT'] ])
                  ->first();
       if (!empty($blasts))
       {
           Log::info("CONTENT  SEND  SMS ::  BLAST FOUND ".print_r($blasts,true )."calling   queuing  process");
           return $blasts;
       }else {
           Log::info("CONTENT  SEND  SMS :: No blast  found for  processing . Process napping  remember to shut  close  window");
           exit();
       }

    }
    public  function   fetch_extra_blast()
    {
       $blasts =  DB::table('blasts')
                  ->select('*') 
                // ->whereRaw('send_time BETWEEN DATE_ADD(NOW(), INTERVAL -1 DAY) AND DATE_ADD(NOW(), INTERVAL 45 MINUTE)')
                  ->where([ ['status','=','UNSENT'] ])
                  ->first();
       if (!empty($blasts))
       {
           Log::info("CONTENT  SEND  SMS ::  BLAST FOUND ".print_r($blasts,true )."calling   queuing  process");
           return $blasts;
       }else {
           Log::info("CONTENT  SEND  SMS :: No blast  found for  processing . Process napping  remember to shut  close  window");
           exit();
       }

    }
    public  function   save_to_outbox($request)
    {
        $insert = "insert into outboxes (blasts_id,reference_number,msisdn,message,status,number_of_retries,created_at,updated_at)";
        $query  = $insert.$request->subs_sql;
        $response = DB::connection()->getpdo()->exec($query);
        $request->total_outboxed =  $response;
        if ( $response > 0 )
        {
            Log::info("CONTENT  SEND  SMS ::  Blast  outboxed successfully . Calling  send  sms  process");
        }
        return $request;
    }
    public  function   send_sms($request)
    {
        $file_name = "send_sms".time().".tsv";
        $path = "/var/data/content_send_sms/$file_name";
        $sql = "SELECT  distinct msisdn , reference_number  FROM outboxes where  blasts_id = $request->id";
        /* Add  Header  To  The  File */
        shell_exec("echo \"
                 $sql
                          \" | mysql -h 35.240.42.252  -u parin -pparin123456789 rt_ussd | sed -E 's/\t+$//g' > $path");

       // shell_exec("sed -i '1imsisdn    reference_number\' $path");
        $request->file_path = $path;
     //   $request = self::fetch_total_sent($request);
        return $request;
    }
    public  function   invoke_send_sms($request)
    {
        $token = RtGateway::getClientToken();
        try {
            $client = new Client([
                'headers' => [
                    'Authorization' =>'Bearer '. $token,
                    'Accept' => 'application/json',
                    'Content-Type' => 'multipart/form-data' ]
            ]);
            $url = "https://proxy.emalify.com/v1/projects/eak9y6r96rn7p0wj/sms/premium/send-from-file";
            $body = [
                [
                    'name'     => 'offerCode',
                    'contents' => $request->offer_code,
                ],
                [
                    'name'     => 'message',
                    'contents' => $request->message,
                ],
                [
                    'name'     => 'messageType',
                    'contents' => 'premium',
                ],
                [
                    'name'     => 'customFile',
                    'contents' => fopen("$request->file_path","r")
                ]
            ];
            $response  = $client->request('POST', $url, ['multipart' => $body]);
            $res=$response->getBody()->getContents();
            return $request;
        }catch (ClientException $e) {
            $response = $e->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            $error = json_decode($responseBodyAsString);
            print_r($error->errors);
            Log::info("Error :: ".print_r($error->errors,true));
        }
    }
    public  function   set_status_queued($request)
    {
        $result = DB::table('blasts')
                  ->where('id','=',$request->id)
                  ->update( [ 'status' => 'PROCESSING'] );
        if ($result > 0 )
        {
            Log::info("CONTENT SEND SMS :: $request->id  has been  queued successfully :: calling  outboxing  process .");
        }
    }
    public  function   set_status_sent($request)
    {
        $result = DB::table('blasts')
            ->where('id','=',$request->id)
            ->update( [
                       'status' => 'SENT' ,
                       'total_sent' => $request->total_outboxed
            ] );
        if ($result > 0 )
        {
            Log::info("CONTENT SEND SMS :: $request->id  has been  SENT successfully.");
            return $request;


        }
    }
}
