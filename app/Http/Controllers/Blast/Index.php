<?php

namespace App\Http\Controllers\Blast;

use App\Blast\Blasts;
use App\Http\Controllers\Bonyeza\Artist;
use App\Http\Controllers\Bonyeza\Content;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Notification\Index as NotificationIndex;
use App\Outbox\Outbox;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Index extends Controller
{
    public function    create(Request $request)
    {
        $rules = [
            'offer_code' => 'required',
          //  'message' =>'required',
            'send_time' => 'required',
         //   'customer_account' => 'required',
            'category_id' => 'required'
        ];
        
        $this->validate($request,$rules);
        $request->customer_account = 6999;
       // Log::info("Scheduled  Request ". print_r($request->all(),true));


        if ($request->category_id == 1 )
        {
            $h = new Helper;
            $request = $h->validate_offercode($request);
           
               $request =  $h-> fetch_service_id($request);
                $request->status = 'MQ';
                $response =  self::store($request);
                $request['blasts_id']  = $response->id;
                $h-> publish_to_mq($request);
                return response()->json($response,200);
            
        }else {
            $rules = [
                       'artist_id' => 'required' ,
                       'content_id' => 'required',
                       'skiza_code' => 'required'
                    ];
            $this->validate($request,$rules);

            $artist = new Artist();
            $request = $artist->fetch_artist_by_id($request);
            $content = new Content();
            $request = $content->fetch_content_by_id($request);

            $request['message'] = "Hi click {$request->link}  to download  $request->title  by  $request->artist_name. Dial *811*$request->skiza_code#";
            $response =  self::store($request);
            $request['blasts_id']  = $response->id;
            $h = new Helper;
            $h-> publish_to_mq($request);
           $notification = new NotificationIndex();
           $notification-> send_sms_notification($request);
            return response()->json($response,200);
        }

    }
    public function    store($request)
    {
        if(isset($request->status))
        {
            $data['status'] = 'SENT';
        }
        $data['is_approved'] = 0;
        $data['offer_code'] = $request->offer_code;
        $data['message'] =  $request->message;
        $data['send_time'] = $request->send_time ;
      //  $data['customer_account'] = $request->customer_account;
        $response = Blasts::create($data);
        return $response;
    }
    public function    send_sms()
    {
           $helper = new Helper();
           $request    =  $helper->fetch_not_sent_blast();
                          $helper->set_status_queued($request);
           $request    =  $helper->fetch_subs($request);
           $request    =  $helper->save_to_outbox($request);
           $request    =  $helper->send_sms($request);
           $request    =  $helper->invoke_send_sms($request);
           $request    =   $helper->set_status_sent($request);
    }
    public function    send_extra_sms()
    {
           $helper = new Helper();
           $request    =  $helper->fetch_extra_blast();
                          $helper->set_status_queued($request);
           $request    =  $helper->fetch_subs($request);
           $request    =  $helper->save_to_outbox($request);
           $request    =  $helper->send_sms($request);
           $request    =  $helper->invoke_send_sms($request);
           $request    =   $helper->set_status_sent($request);
    }
    public function    per_account($customer_account)
    {
        $blasts =DB::table('blasts')
            ->select('blasts.id','send_time','alias','message','status')
            ->join('shortcode_services','blasts.offer_code','=','shortcode_services.offer_code')
            ->where([
                ['blasts.customer_account','=',$customer_account],
                ['status','=','SENT']
            ])
          //  ->whereNotIn('shortcode_services.service_id',[10800,667250,10780,6690,643,1201,1200])
            ->orderBy('blasts.send_time','desc')
            ->limit('500')
            ->get();
        return response()->json($blasts,200);
    }
    public function   scheduled_sms($customer_account)
    {
        $date = Carbon::now('Africa/Nairobi');
        $blasts =DB::table('blasts')
            ->select('blasts.id','send_time','alias','message','is_approved','remarks')
        //    -
            ->join('shortcode_services','blasts.offer_code','=','shortcode_services.offer_code')
            ->where([
            //    ['blasts.customer_account','=',$customer_account],
                ['send_time','>',$date],
            ])
            ->whereIn('status',['SENT'])
         //   ->whereNotIn('shortcode_services.service_id',[10800,667250,10780,6690,643,1201,1200])
            ->orderBy('blasts.updated_at','desc')
            ->get();
        return response()->json($blasts,200);
    }
    public function delete(Request  $request)
    {
        $rules = [
            'id' => 'required'
        ];
        $this->validate($request,$rules);
    
        $result = DB::table('blasts')
            ->where('id','=',$request->id)
            ->update(['status' => 'cancelled']);
        if ($result == 1) {
            return response()->json('Cancelled  Successfully',200);
        }
       return response()->json('Error In Cancellation',200);
    }
    public  function   approve(Request $request)
    {
        $rules = [
            'id' => 'required',
            'remarks' => 'required',
            'message' => 'required',
            'send_time' => 'required'
        ];
        $this->validate($request,$rules);

        $result = DB::table('blasts')
        ->where('id','=',$request->id)
        ->update(
                   [
                       'is_approved' => '1',
                       'remarks' => $request->remarks,
                       'message' => $request->message,
                       'send_time' => $request->send_time
                   ]
            );
    }
    public  function   reject(Request $request)
    {
        $rules = [
            'id' => 'required',
            'remarks' => 'required',
        ];
        $this->validate($request,$rules);
        $result = DB::table('blasts')
        ->where('id','=',$request->id)
        ->update(
                   [
                       'is_approved' => '2',
                       'remarks' => $request->remarks,
                   ]
            );
            if($result){
                return response()->json("Disapproved Successfully",200);
            }else {
                return response()->json("Failed To Disapprove",200);
            }
    }
}
