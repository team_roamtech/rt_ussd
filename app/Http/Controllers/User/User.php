<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use App\User as AppUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class User extends Controller
{
    public function  create(Request $request)
    {
      $rules = [
        'full_name' => 'required | unique:users,full_name',
        'stage_name' => 'required | unique:users,stage_name',
        'phone_number' => 'required | unique:users,phone_number',
        'email' => 'required |email|unique:users',
       // 'password' => 'required|min:6 | confirmed'
  ];
   $this->validate($request,$rules);
      $data['full_name']=$request['full_name'];
      $data['customer_account'] =  mt_rand(10,200);
      $data['stage_name'] = $request['stage_name'];
      $data['phone_number'] = $request['phone_number'];
      $data['email'] = $request['email'];
      $response = self::store($data);
      $request['user_id'] = $response->id;
      return $request;
    }
    public  function store($data)
    {
      $response=\App\User::create($data);
       return $response;
    }
    public function fetch_all_users()
    {
      $all_users = DB::table('users')
      ->select('full_name','id')
      ->get();
      return $all_users;
    }
    public function    msisdn ($user_id)
    {
      $all_users = DB::table('users')
      ->select('msisdn')
      ->where([
        ['id','=',$user_id]
         ])
      ->get();
      return response()->json();
    }
} 
