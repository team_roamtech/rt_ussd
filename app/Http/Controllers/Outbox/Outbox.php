<?php

namespace App\Http\Controllers\Outbox;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Outbox extends Controller
{
    public  function  store($request)
    {
         $outbox =new  \App\Outbox\Outbox();
         $response = $outbox->insert($request);
         return $response;
    }
}
