<?php

namespace App\Http\Controllers\Shortcodes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShortcodeService extends Controller
{
    public  function  store(Request $request)
    {
        $rules = [
            'shortcodes_id' => 'required',
            'offer_code' => 'required',
            'customer_account'=>'required',
            'sdp_name' => 'required',
            'start_keyword'=>'required',
            'stop_keyword' => 'required',
            'price'=>'required',
            'alias' => 'required'
        ];
        $this->validate($request,$rules);

        $data ['shortcodes_id'] = $request->shortcodes_id;
        $data['offer_code'] =     $request->offer_code;
        $data['customer_account'] = $request->customer_account;
        $data['service_id'] = $request->service_id;
        $data['price'] =    $request->price;
        $data['sdp_name'] = $request->sdp_name;
        $data['start_keyword'] = $request->start_keyword;
        $data['stop_keyword'] = $request->stop_keyword;
        $data['endpoint'] = $request->endpoint;
        if (isset($request->spd_service_id))
        {
            $data['sdp_service_id'] = $request->spd_service_id;
        }
        if (isset($request->sdp_spid))
        {
            $data['sdp_id'] = $request->sdp_service_id;
        }

        $data['alias'] = $request->alias;

        $response = \App\Shortcode\ShortcodeService::create($data);
        return $response;
     }
     public function  fetch_services($customer_account)
     {
         $res= DB::table('shortcode_services')
             ->select('shortcode_services.offer_code','alias')
             ->join('clean_subs', 'clean_subs.offer_code', '=', 'shortcode_services.offer_code')
             ->where( [ ['customer_account','=',$customer_account]])
             ->whereNotNull('offer_code')
             ->whereNotIn('service_id',[10800,667250,10780,6690,643,1201])
             ->addSelect(DB::raw('count(DISTINCT shortcode_services.alias) as count'))
             ->groupBy('shortcode_services.alias')
             ->get();
         return response()->json($res);
     }
    public  function   fetch_per_account($customer_account)
    {
        $res= DB::select("
         select  CONCAT(alias,'(',count(*),')') as alias , shortcode_services.offer_code
         from clean_subs
         inner  join shortcode_services  on  clean_subs.offer_code = shortcode_services.offer_code
         group by alias,shortcode_services.offer_code");
        return response()->json($res);
    }
    public function fetch_all()
    {
       $shortcode_service = DB::table('shortcode_services')
                            ->select('sdp_name','id')
                            ->get();
                            return $shortcode_service;
    }

}
