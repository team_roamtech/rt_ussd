<?php

namespace App\Http\Controllers\Shortcodes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Shortcode extends Controller
{
    public function  store(Request $request)
    {
        $rules = [
            'shortcode' => 'required',
            'customer_account' =>'required'
        ];
        $this->validate($request,$rules);
        $resource = self::create($request);
        return $resource;
    }
    public  function  create($request)
    {
        $data['shortcode'] = $request->shortcode;
        $data['customer_account'] = $request->customer_account;
        $resource =\App\Shortcode\Shortcode::create($data);
        return $resource;
    }
}
