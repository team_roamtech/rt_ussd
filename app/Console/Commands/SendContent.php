<?php

namespace App\Console\Commands;

use App\Http\Controllers\Blast\Index;
use Illuminate\Console\Command;

class SendContent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:content';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dispatch  the  message  to   content send sms';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $blasts = new  Index();
        $blasts->send_sms();
    }
}
