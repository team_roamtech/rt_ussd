<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Blast\Index;

class SendExtra extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:extra';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send  an extra  message';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $blasts = new  Index();
        $blasts->send_extra_sms();
    }
}
