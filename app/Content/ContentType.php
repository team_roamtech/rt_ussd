<?php

namespace App\Content;

use Illuminate\Database\Eloquent\Model;

class ContentType extends Model
{
    protected  $guarded = [];
}
