<?php

namespace App\Content;

use Illuminate\Database\Eloquent\Model;

class ContentDetail extends Model
{
    protected $guarded = [];
}
