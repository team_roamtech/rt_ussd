<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShortcodeServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shortcode_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('shortcodes_id');
            $table->text('offer_code');
            $table->integer('price');
            $table->mediumText('sdp_name');
            $table->bigInteger('customer_account');
            $table->string('start_keyword');
            $table->string('stop_keyword');
            $table->mediumText('endpoint')->nullable();
            $table->integer('sdp_service_id')->nullable();
            $table->integer('sdp_spid')->nullable();
            $table->mediumText('alias');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shortcode_services');
    }
}
