<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blasts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('offer_code');
            $table->text('message');
            $table->string('status')->nullable()->default('NOT_SENT');
            $table->dateTime('send_time');
            $table->integer('delivered')->nullable()->default('0');
            $table->integer('total_sent')->nullable()->default('0');
            $table->integer('number_of_sends')->nullable()->default('0');
            $table->integer('customer_account');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blasts');
    }
}
