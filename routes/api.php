<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'prefix' => 'content'
],function () {
    Route::post('create/type','Content\ContentType@store');
    Route::get('read/type','Content\ContentType@read');
    Route::get('all/types','Content\ContentType@fetch_all');
    Route::post('delete/type','Content\ContentType@delete');
    Route::post('upload','Upload\Upload@upload');
});

Route::group([
    'prefix' => 'content'
],function () {
    Route::get('fetch/all','Content\Content@fetch_all');
});

Route::group([
    'prefix' => 'content'
],function () {
    Route::post('create/detail','Content\ContentDetail@create');
});

Route::group([
    'prefix' => 'ussd'
],function () {
    Route::post('create','Ussd\Ussd@store');
    Route::get('fetch/all','Ussd\Ussd@fetch_all');
    Route::get('read/{customer_account}','Ussd\Ussd@read_per_acc');
});
Route::group([
    'prefix' => 'ussd'
],function () {
    Route::post('type','Ussd\UssdType@store');
});
Route::group([
    'prefix' => 'shortcode'
],function () {
    Route::post('create','Shortcodes\Shortcode@store');
    Route::post('service/create','Shortcodes\ShortcodeService@store');
    Route::get('{customer_account}/read','Shortcodes\ShortcodeService@fetch_per_account');
    Route::get('fetch/all','Shortcodes\ShortcodeService@fetch_all');
});
Route::group([
    'prefix' => 'blast'
],function () {
    Route::post('create','Blast\Index@create');
    Route::post('send','Blast\Index@send_sms');
    Route::get('{customer_account}/fetch','Blast\Index@per_account');
    Route::get('scheduled/{customer_account}','Blast\Index@scheduled_sms');
    Route::post('delete','Blast\Index@delete');
    Route::post('approve','Blast\Index@approve');
    Route::post('reject','Blast\Index@reject');
});

Route::group([
    'prefix' => 'user'
],function () {
    Route::post('create','User\User@create');
    Route::get('fetch/all','User\User@fetch_all_users');
});

Route::group([
    'prefix' => 'stats'
],function () {
    Route::get('{id}/count','Stats\Index@fetch_stats');
});


 Route::get('d/{download_name}','Download\Index@download_music');
 Route::get('skiza','Skiza\Index@link');



 Route::group([
    'prefix' => 'artist'
],function () {
    Route::get('all','Bonyeza\Artist@all');
    Route::get('{user_id}/content','Bonyeza\Content@fetch_by_id');
});



Route::group([
    'prefix' => 'notification'
],function () {
    Route::post('initiate','Notification\Index@send_sms_notification');
});



Route::group([
    'prefix' => 'bonyeza'
],function () {
    Route::post('generator','Bonyeza\App@generate_link');
    Route::get('download/{id}','Download\Index@download_v2');
    Route::post('register','Download\App@register_music');
});


Route::get('{id}','Download\Index@download_v2');